#include <QMainWindow>

namespace Ui {
class Notepad;
}

class Notepad : public QMainWindow
{
	Q_OBJECT

public:
	 Notepad(QWidget *parent = 0);
	~Notepad();

private slots:
	 void newFile();
	 void openFile();
	 void save();
	 void saveAs();
	 void copy();
	 void cut();
	 void paste();
	 void font();

private:
	Ui::Notepad *ui;
	QString currentFile;
};
