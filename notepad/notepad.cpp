#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <QFont>
#include <QFontDialog>

#include "notepad.h"
#include "ui_notepad.h"

Notepad::Notepad(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::Notepad)
{
	ui->setupUi(this);

	connect(ui->newFileButton  , SIGNAL(clicked(bool)), this, SLOT(newFile()));
	connect(ui->openFileButton , SIGNAL(clicked(bool)), this, SLOT(openFile()));
	connect(ui->saveButton     , SIGNAL(clicked(bool)), this, SLOT(save()));
	connect(ui->saveAsButton   , SIGNAL(clicked(bool)), this, SLOT(saveAs()));
	connect(ui->copyButton     , SIGNAL(clicked(bool)), this, SLOT(copy()));
	connect(ui->cutButton      , SIGNAL(clicked(bool)), this, SLOT(cut()));
	connect(ui->pasteButton    , SIGNAL(clicked(bool)), this, SLOT(paste()));
	connect(ui->fontButton     , SIGNAL(clicked(bool)), this, SLOT(font()));
}

Notepad::~Notepad()
{
	delete ui;
}

void Notepad::newFile()
{
	currentFile.clear();
	ui->textEdit->setText(QString());
}

void Notepad::openFile()
{
	QString fileName = QFileDialog::getOpenFileName(this, "Open the file");
	QFile file(fileName);
	currentFile = fileName;
	if (!file.open(QIODevice::ReadOnly | QFile::Text))
		return;
	setWindowTitle(fileName);
	QTextStream in(&file);
	QString text = in.readAll();
	ui->textEdit->setText(text);
	file.close();
}

void Notepad::save()
{
	QString fileName;
	if (currentFile.isEmpty()) {
		fileName = QFileDialog::getSaveFileName(this, "Save");
		currentFile = fileName;
	} else {
		fileName = currentFile;
	}
	QFile file(fileName);
	if (!file.open(QIODevice::WriteOnly | QFile::Text))
		return;

	setWindowTitle(fileName);
	QTextStream out(&file);
	QString text = ui->textEdit->toPlainText();
	out << text;
	file.close();
}

void Notepad::saveAs()
{
	QString fileName = QFileDialog::getSaveFileName(this, "Save as");
	QFile file(fileName);

	if (!file.open(QFile::WriteOnly | QFile::Text))
		return;

	currentFile = fileName;
	setWindowTitle(fileName);
	QTextStream out(&file);
	QString text = ui->textEdit->toPlainText();
	out << text;
	file.close();
}

void Notepad::copy()
{
	ui->textEdit->copy();
}

void Notepad::cut()
{
	ui->textEdit->cut();
}

void Notepad::paste()
{
	ui->textEdit->paste();
}

void Notepad::font()
{
	bool fontSelected;
	QFont font = QFontDialog::getFont(&fontSelected, this);
	if (fontSelected)
		ui->textEdit->setFont(font);
}
